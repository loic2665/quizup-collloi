<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */

require_once(__DIR__ . "/php/functions/user.php");
redirectIfLoggedIn();
?>


<html>
<head>
    <title>Connexion</title>
    <?php require(__DIR__ . "/inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/inc/nav.php"); ?>

<section>

    <div id="serverAnswer">

    </div>

    <?php


    $fields = array(
        array(
            "label" => "Email",
            "type" => "email",
            "id" => "email",
            "placeholder" => "Votre email",

        ),
        array(
            "label" => "Mot de passe",
            "type" => "password",
            "id" => "password",
            "placeholder" => "Votre mot de passe (au moins un caractère majuscule, minucule, chiffre, 8 caract. min)",

        ),
    );
    ?>

    <fieldset>
        <legend>Connexion</legend>

        <?php foreach ($fields as $field) { ?>

            <?php generateInput($field); ?>

        <?php } ?>



        <button type="submit" class="login btn btn-primary">Se connecter</button>
    </fieldset>


</section>


<?php include(__DIR__ . "/inc/footer.php"); ?>

<?php generateAjax("login", $fields, "/php/login.php", 2000, "/social/profile.php"); ?>


</body>
</html>
