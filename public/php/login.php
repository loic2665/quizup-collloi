<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 14:11
 */

@session_start();

require_once(__DIR__ . "/functions/validator.php");
require_once(__DIR__ . "/functions/json.php");
require_once(__DIR__ . "/functions/user.php");
require_once(__DIR__ . "/database/connect.php");

$champs = array( // ajouter/supprimer les entrees en fonction des champs du formulaire
    "email",
    "password"
);

$success = true;

$input = array();

$answer = array(
    "success" => false,
    "message" => ""
);

if (isLoggedIn()) {

    $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Vous êtes déja connecté !</div>";
    $answer["success"] = false;
    encodeAndSendJson($answer);
    die();
}


/*
 *
 * Vérification des champs (regex)
 *
 *
 * */


$errors = "<div class='alert alert-danger'><strong>Oups !</strong> Une ou plusieurs erreurs se sont produite.";
$i = 0;
foreach ($champs as $champ) {
    if (isset($_POST[$champ]) or $_POST[$champ] != "") {
        if (!checkInput($_POST[$champ], $champ)) {
            $success = false;
            $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est invalide.";
            $input[$champ] = "";
        } else {
            $input[$champ] = htmlspecialchars(addslashes($_POST[$champ]));
        }
    } else {
        $success = false;
        $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est manquant. (ajax ?).";
    }

}
$errors .= "</div>";
$answer["message"] = $errors;


/*
 *
 * Fin vérifiaction des champs
 *
 * */




if ($success) {

    $result = mysqli_query($bdd, "SELECT count(*) AS nAccount FROM `usersession` WHERE `email` = '" . $input["email"] . "'");
    $tab = $result->fetch_array();

    if ($tab["nAccount"] == 0) {
        $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Votre email est inexistant.</div>";
    } elseif ($tab["nAccount"] > 1) {
        $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Euh, une erreur s'est produite, impossible de vous connecter.</div>";

    } else {
        $result = mysqli_query($bdd, "SELECT * FROM `usersession` WHERE email LIKE '" . $input["email"] . "'");
        $tab = $result->fetch_array();
        if ($tab["password"] != hash("sha256", $input["password"])) {
            $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Le mot de passe entré n'est pas correct.</div>";
        } else {

            $status = createUserSession($tab);
            if ($status) {
                $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Une erreur s'est produite lors de la connexion.</div>";

            } else {

                $answer["message"] = "<div class='alert alert-success'><strong>Bravo !</strong> Connexion reussie ! Redirection...</div>";
                $answer["success"] = true;
            }

        }

    }
}
encodeAndSendJson($answer);


?>
