<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 14:11
 */

@session_start();

require_once(__DIR__ . "/functions/validator.php");
require_once(__DIR__ . "/functions/json.php");
require_once(__DIR__ . "/functions/user.php");
require_once(__DIR__ . "/database/connect.php");


$fields = array(
    array(
        "id" => "username",

    ),
    array(
        "id" => "ville",

    ),
    array(
        "id" => "bio",

    ),
    array(
        "id" => "lang",

    ),
    array(
        "id" => "prive",

    ),
);

$champs = array( // ajouter/supprimer les entrees en fonction des champs du formulaire
    "username",
    "ville",
    "bio",
    "lang",
    "prive"
);

$success = true;

$input = array();

$answer = array(
    "success" => false,
    "message" => ""
);

if (!isLoggedIn()) {

    $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Vous n'êtes pas connecté !</div>";
    $answer["success"] = false;
    encodeAndSendJson($answer);
    die();
}




/*
 *
 * Vérification des champs (regex)
 *
 *
 * */


$errors = "<div class='alert alert-danger'><strong>Oups !</strong> Une ou plusieurs erreurs se sont produite.";
$i = 0;
foreach ($champs as $champ) {
    if (isset($_POST[$champ]) || empty($_POST[$champ])) {
        if (!checkInput($_POST[$champ], $champ)) {
            $success = false;
            $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est invalide.";
            $input[$champ] = "";
        } else {
            $input[$champ] = htmlspecialchars(addslashes($_POST[$champ]));
        }
    } else {
        $success = false;
        $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est manquant. (ajax ?).";
    }

}
$errors .= "</div>";
if(!$success){
    $answer["message"] = $errors;
}


/*
 *
 * Fin vérifiaction des champs
 *
 * */

if($input["prive"] == "true"){
    $prive = 1;

}else{
    $prive = 0;
}


$result = mysqli_query($bdd, "UPDATE profil SET nomProfil = '".$input["username"]."', villeOrigine = '".$input["ville"]."', bio = '".$input["bio"]."', profilPrive = ".$prive." WHERE idProfil = ".$_SESSION["idProfil"].";");

$answer["message"] = "<div class='alert alert-success'><strong>Cool !</strong> Vos données sont bien mis à jour.</div>";

encodeAndSendJson($answer);


?>
