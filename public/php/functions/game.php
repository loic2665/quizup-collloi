<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 24/03/19
 * Time: 15:14
 */

require_once(__DIR__ . "/../../php/database/connect.php");
require_once(__DIR__ . "/../../php/functions/json.php");

function initGame($theme)
{

    @session_start();

    resetGame();

    $_SESSION["game"]["numQuestion"] = 0;
    $_SESSION["game"]["timeStampStart"] = time();
    $_SESSION["game"]["lastAnswerTimeStamp"] = time();
    $_SESSION["game"]["endGame"] = false;
    $_SESSION["game"]["userAnswer"] = array();
    $_SESSION["game"]["idTheme"] = $theme;
    $_SESSION["game"]["totalPoints"] = 0;
    $_SESSION["game"]["fromGame"] = false;


    $status = pickQuestions($theme);

    if (!$status["success"]) {
        return false;
    } else {
        $_SESSION["game"]["content"] = $status;
        $_SESSION["game"]["nomTheme"] = getThemeDetailsById($theme)["details"]["nom"];
    }
    updateLastTime();

    return true;


}

function resetGame()
{

    @session_start();

    $_SESSION["game"] = array();

}

function pickQuestions($theme)
{

    global $bdd;
    @session_start();
    $questions = array();

    $result = mysqli_query($bdd, "SELECT * FROM `question` WHERE `idtheme` = " . $theme . " ORDER BY RAND() LIMIT 7;");


    if ($result->num_rows == 7) {
        $questions["success"] = true;
        $id = 0;
        while ($row = mysqli_fetch_array($result)) {


            $questions[$id]["idQuestion"] = $row["idQuestion"];
            $questions[$id]["image"] = $row["Illustration"];
            $questions[$id]["question"] = $row["libelleQuestion"];
            $questions[$id]["answers"] = array();


            $cols = array(
                "answer",
                "distracteur01",
                "distracteur02",
                "distracteur03",
            );

            $colsLetter = array(
                "a",
                "b",
                "c",
                "d"
            );

            $used = array();
            $jId = 0;
            while (!(in_array(0, $used) && in_array(1, $used) && in_array(2, $used) && in_array(3, $used))) {

                $rand = rand(0, 3);
                $lettre = $colsLetter[$rand];
                if (!in_array($rand, $used)) {
                    array_push($used, $rand);
                    if ($cols[$rand] == "answer") {
                        $questions[$id]["answers"][$jId]["lettre"] = $lettre;
                        $questions[$id]["answers"][$jId]["content"] = $row["answer"];
                        $questions[$id]["answers"][$jId]["valid"] = true;
                        $jId++;
                    } else {

                        $questions[$id]["answers"][$jId]["lettre"] = $lettre;
                        $questions[$id]["answers"][$jId]["content"] = $row[$cols[$rand]];
                        $questions[$id]["answers"][$jId]["valid"] = false;
                        $jId++;
                    }
                }

            }
            $id++;

        }


    } else {
        $questions["success"] = false;
        $questions["details"] = "Le thème n'a pas été trouvé ou il faut au moins 7 questions.";
    }

    return $questions;

}

// IN GAME FUNCTIONS

function isAnswerAllowed($rep)
{

    return in_array($rep, array(0, 1, 2, 3, "0", "1", "2", "3"));

}

function checkAnswer($rep)
{

    @session_start();
    $nQuestion = $_SESSION["game"]["numQuestion"];
    return $_SESSION["game"]["content"][$nQuestion]["answers"][$rep]["valid"];

}

function insertUserAnswer($answer, $points)
{

    @session_start();
    $nQuestion = $_SESSION["game"]["numQuestion"];

    $_SESSION["game"]["userAnswers"][$nQuestion]["reponse"] = $answer;
    $_SESSION["game"]["userAnswers"][$nQuestion]["points"] = $points;


}

function updateLastTime()
{

    @session_start();
    $_SESSION["game"]["lastAnswerTimeStamp"] = time();

}

function calculPoints()
{

    @session_start();
    $multiplicateur = getBonusBoost($_SESSION["idProfil"]);
    $now = time();
    $oldTime = $_SESSION["game"]["lastAnswerTimeStamp"];

    $nbPoints = 12;

    $points = ($nbPoints - ($now - $oldTime))*$multiplicateur;
    if ($points < 0) {
        $points = 0;
    }

    if ($points > 10) {
        $points = 10;
    }

    if ($_SESSION["game"]["numQuestion"] > 5) {
        $points *= 7;

    }

    $points = round($points * $multiplicateur, 0);

    $_SESSION["game"]["totalPoints"] += $points;

    return $points;

}

function updateNextQuestion()
{

    @session_start();
    $_SESSION["game"]["numQuestion"] += 1;
    return $_SESSION["game"]["numQuestion"];

}

function isEndGame()
{


    @session_start();
    $nQuestion = $_SESSION["game"]["numQuestion"];

    if ($nQuestion > 6) {

        $_SESSION["game"]["fromGame"] = true;
        return true;
    }
    return false;

}

function pickNextQuestion()
{

    @session_start();
    $answer = array();

    $nQuestion = $_SESSION["game"]["numQuestion"];
    $questAnswers = $_SESSION["game"]["content"][$nQuestion]["answers"];


    $i = 0;
    foreach ($questAnswers as $ans) {
        $answer[$i]["content"] = $ans["content"];
        $i++;
    }

    return $answer;

}

// END GAME

function sendAllGameDetails()
{

    global $bdd;
    @session_start();
    $idProfil = $_SESSION["idProfil"];

    $joursConsecutifs = getJoursConsecutifs($idProfil);


    $bonusBoost = getBonusBoost($idProfil);


    mysqli_query($bdd, "INSERT INTO `partie` VALUES (NULL, '" . date("Y/m/d H:i:s", time()) . "');");


    $result = mysqli_query($bdd, "SELECT max(idPartie) as idPartie FROM partie;");

    $idPartie = mysqli_fetch_array($result)["idPartie"];

    mysqli_query($bdd, "INSERT INTO `participer` VALUES (" . $idProfil . ", " . $idPartie . ", " . $bonusBoost . ");");


    $i = 0;
    unset($_SESSION["game"]["content"]["success"]);
    foreach ($_SESSION["game"]["content"] as $question) {

        $ordreLettrePresente = "";


        foreach ($question["answers"] as $reponse) {

            $ordreLettrePresente .= $reponse["lettre"];

        }

        $idQuestion = $question["idQuestion"];


        $result = mysqli_query($bdd, "INSERT INTO integrer VALUES (" . $idPartie . "," . $idQuestion . ", " . $i . ", '" . $ordreLettrePresente . "');");

        $iRep = $_SESSION["game"]["userAnswers"][$i]["reponse"];
        $nbPts = $_SESSION["game"]["userAnswers"][$i]["points"];
        mysqli_query($bdd, "INSERT INTO `repondre` VALUES (" . $idProfil . ", " . $idPartie . ", " . $idQuestion . ", '" . str_split($ordreLettrePresente)[$iRep] . "', " . $nbPts . ");");


        $i++;

    }

    return $idPartie;


}

// parties

function doesThisPartieExist($id){

    global $bdd;
    @session_start();
    $result = mysqli_query($bdd, "SELECT count(*) as nbPartie FROM partie WHERE idPartie = ".$id.";");

    if (mysqli_fetch_array($result)["nbPartie"] == 1) {
        return true;
    }
    return false;

}

function getAllQuestionsFromPartieId($id){

    global $bdd;
    @session_start();
    $result = mysqli_query($bdd, "SELECT * FROM partie JOIN integrer i on partie.idPartie = i.idPartie JOIN repondre r on partie.idPartie = r.idPartie JOIN question q on r.idQuestion = q.idQuestion AND q.idQuestion = i.idQuestion JOIN participer p on partie.idPartie = p.idPartie WHERE partie.idPartie = ".$id." ORDER BY i.numero");

    $answer = array();

    $i = 0;


    $dictAnswer = array(

        "a" => "answer",
        "b" => "distracteur01",
        "c" => "distracteur02",
        "d" => "distracteur03",

    );
    $answer["totalPoints"] = 0;
    while ($row = mysqli_fetch_array($result)) {

        if($i == 0){

            $answer["bonusBoost"] = $row["bonusBoost"];
            $answer["idProfil"] = $row["idProfil"];
            $answer["idPartie"] = $row["idPartie"];
            $answer["datePartie"] = $row["timestampPartie"];
            $answer["themeData"] = getThemeDetailsById($row["idTheme"]);


        }

        $answer["questions"][$i]["idQuestion"] = $row["idQuestion"];
        $answer["questions"][$i]["textQuestion"] = $row["libelleQuestion"];
        $answer["questions"][$i]["image"] = $row["Illustration"];
        $answer["questions"][$i]["reponse"] = $row["reponse"];
        $answer["questions"][$i]["points"] = $row["points"];
        $answer["totalPoints"] += $row["points"];

        $j = 0;

        $ordreRep = str_split($row["ordreReponses"]);

        foreach ($ordreRep as $letter){

            $answer["questions"][$i]["answers"][$j]["content"] = $row[$dictAnswer[$letter]];
            $answer["questions"][$i]["answers"][$j]["letter"] = $letter;

            if($dictAnswer[$letter] == "answer"){
                $answer["questions"][$i]["answers"][$j]["valid"] = true;
            }else{
                $answer["questions"][$i]["answers"][$j]["valid"] = false;
            }

            if($row["reponse"] == $letter){
                $answer["questions"][$i]["answers"][$j]["selected"] = true;
            }else{
                $answer["questions"][$i]["answers"][$j]["selected"] = false;
            }

            if($row["reponse"] == "a"){

                $answer["questions"][$i]["reussi"] = true;

            }else{

                $answer["questions"][$i]["reussi"] = false;
            }

            $j++;

        }


        $i++;
    }

    //die(json_encode($answer));
    return $answer;



}