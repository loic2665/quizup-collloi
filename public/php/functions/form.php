<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 12:05
 */

function generateInput($field)
{ ?>


    <?php if ($field["type"] == "select") { ?>

    <div class="form-group">
        <label for="<?php echo($field["id"]); ?>"><?php echo($field["label"]); ?></label>
        <select class="custom-select" id="<?php echo($field["id"]); ?>">
            <option disabled selected="" value="none"><?php echo($field["default"]);?></option>
            <?php foreach ($field["options"] as $option) { ?>
                <option value="<?php echo($option); ?>">
                    <?php echo($option); ?>
                </option>
            <?php } ?>
        </select>
    </div>
<?php } else if ($field["type"] == "textarea") { ?>

    <div class="form-group">
        <label for="<?php echo($field["id"]); ?>"><?php echo($field["label"]); ?></label>
        <textarea class="form-control" id="<?php echo($field["id"]); ?>"
                  rows="<?php echo($field["size"]); ?>"></textarea>
    </div>


<?php } else { ?>
    <div class="form-group">
        <label for="<?php echo($field["id"]); ?>"><?php echo($field["label"]); ?></label>
        <input type="<?php echo($field["type"]); ?>" class="form-control" id="<?php echo($field["id"]); ?>"
               placeholder="<?php echo($field["placeholder"]); ?>">
    </div>
<?php } ?>


<?php } ?>


