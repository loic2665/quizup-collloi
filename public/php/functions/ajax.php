<?php function generateAjax($button, $fields, $urlForm, $sleep, $urlRedirect){ ?>

    <script>

        $(".<?php echo($button); ?>").click(function () {
            $.post("<?php echo($urlForm); ?>",
                {
            <?php foreach ($fields as $field){ ?>
                <?php if($field["id"] == "prive"){ ?>
                    <?php echo($field["id"]) ?>: $('#<?php echo($field["id"]) ?>').is(":checked"),
                <?php }else{ ?>
                    <?php echo($field["id"]) ?>: $('#<?php echo($field["id"]) ?>').val(),
                <?php } ?>
            <?php } ?>
        },

            function (data, status) {

                data = JSON.parse(data);
                document.getElementById("serverAnswer").innerHTML = data["message"];

                var element = document.getElementById("serverAnswer");
                if (data["success"]) {


                    sleep(<?php echo($sleep); ?>).then(() => {
                        // Do something after the sleep!
                        window.location.replace("<?php echo($urlRedirect); ?>");
                    });
                }

            }

        )
            ;
        });

    </script>

<?php } ?>

