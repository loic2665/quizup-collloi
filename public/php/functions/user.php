<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:15
 */


require(__DIR__ . "/../database/connect.php");

function isLoggedIn()
{

    @session_start();
    if (isset($_SESSION["idProfil"])) {
        return true;
    }
    return false;

}

function redirectIfnotLoggedIn()
{
    if (!isLoggedIn()) {
        header("Location: /login.php");
        die();
    }
}

function redirectIfLoggedIn()
{
    if (isLoggedIn()) {
        header("Location: /index.php");
        die();
    }
}

function createUserSession($tab)
{ // return 1 => erreur, sinon 0.

    @session_start();
    global $bdd;

    $_SESSION["idProfil"] = $tab["idProfil"];
    $now = time();
    $joursConsecutifs = $tab["joursConsecutifs"];
    if ($joursConsecutifs > 0) {

        $lastActivity = strtotime(date("Y/m/d 00:00:00", strtotime($tab["lastActivity"])));

        if (time() > $lastActivity &&
            time() < ($lastActivity + 86400 * 2) &&
            (date("d", $now) != date("d", strtotime($tab["lastActivity"])))
        ) {
            $joursConsecutifs++;
        } elseif (($now - strtotime($lastActivity) > (86400 * 2)) &&
            (date("d", $now) != date("d", strtotime($tab["lastActivity"])))
        ) {
            $joursConsecutifs = 0;
        }
    } else {
        $joursConsecutifs = 1;
    }

    updateUserLastActivity($tab["idProfil"]);
    $result = mysqli_query($bdd, "UPDATE `usersession` SET `joursConsecutifs`='" . $joursConsecutifs . "' WHERE `idProfil` = " . $tab["idProfil"] . ";");


    return 0;


}

function logoutUser()
{
    @session_start();
    if (isset($_SESSION["idProfil"])) {
        $_SESSION["idProfil"] = NULL;
        return true;
    }
    return false;
}

function updateUserLastActivity($id)
{

    global $bdd;
    $result = mysqli_query($bdd, "UPDATE `usersession` SET `lastActivity`='" . date("Y/m/d H:i:s", time()) . "' WHERE `idProfil` = " . $id . ";");


}


function getProfileDetails($id)
{

    global $bdd;
    @session_start();
    $result = mysqli_query($bdd, "SELECT * FROM `profil` WHERE `idProfil` = " . $id . ";");

    if (!$result->num_rows) {
        $answer = array(
            "success" => false,
            "error" => "Profil introuvable."
        );
    }else {

        $profil = $result->fetch_array();

        $answer = array(
            "success" => true,
            "details" => array(
                "id" => $profil["idProfil"],
                "private" => $profil["profilPrive"],
                "username" => $profil["nomProfil"],
                "photo" => $profil["photoProfil"],
                "facade" => $profil["photoFacade"],
                "ville" => $profil["villeOrigine"],
                "bio" => $profil["bio"],
                "langue" => $profil["langue"],
                "titre" => getTitreById($profil["idTitre"]),
                "pays" => getPaysById($profil["idPays"]),
                "region" => getRegionById($profil["idRegion"]),
            ),
            "blockedByUser" => isHeBlockedByUser($_SESSION["idProfil"], $profil["idProfil"]),
            "isHeBlockingHim" => isHeBlockingHim($_SESSION["idProfil"], $profil["idProfil"]),
            "isFollowingHim" => isHeFollowingHim($_SESSION["idProfil"], $profil["idProfil"]),
            "isHeFollowHim" => isHeFollowHim($_SESSION["idProfil"], $profil["idProfil"]),
            "isPersonnalProfile" => isPersonnalProfile($profil["idProfil"])
        );

    }
    return $answer;


}

function getTitreById($id)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `titre` WHERE `idTitre` = " . $id . ";");
    $exist = @$result->num_rows;
    if ($exist) {
        $titre = $result->fetch_array();
        return $titre["libelleTitre"];
    } else {
        return "Erreur lors de la récupération du titre.";
    }
}

function getPaysById($id)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `pays` WHERE `idPays` = " . $id . ";");
    $exist = @$result->num_rows;
    if ($exist) {
        $pays = $result->fetch_array();
        return $pays["libellePays"];
    } else {
        return "Erreur lors de la récupération du pays.";
    }
}

function getRegionById($id)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `region` WHERE `idRegion` = " . $id . ";");
    $exist = @$result->num_rows;
    if ($exist) {
        $region = $result->fetch_array();
        return $region["libelleRegion"];
    } else {
        return "Erreur lors de la récupération de la région.";
    }
}

function isHeBlockedByUser($visiteur, $profil)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `bloquer` WHERE `idProfil` = " . $profil . " AND `idProfil_1` = " . $visiteur . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;
}

function isHeBlockingHim($visiteur, $profil)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `bloquer` WHERE `idProfil` = " . $visiteur . " AND `idProfil_1` = " . $profil . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;
}


function isHeFollowingHim($visiteur, $profil)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `s_abonner` WHERE `idProfil` = " . $profil . " AND `idProfil_1` = " . $visiteur . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;
}

function isHeFollowHim($visiteur, $profil)
{
    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `s_abonner` WHERE `idProfil` = " . $visiteur . " AND `idProfil_1` = " . $profil . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;
}

function isPersonnalProfile($id)
{
    @session_start();
    if (isset($_SESSION["idProfil"]) && $_SESSION["idProfil"] == $id) {
        return true;
    }
    return false;
}

function doesThisPlayerExist($profil)
{

    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `profil` WHERE `idProfil` = " . $profil . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;

}

function getAllBlockedProfileByUser()
{

    global $bdd;
    @session_start();
    $result = mysqli_query($bdd, "SELECT * FROM `bloquer` JOIN `profil` ON (bloquer.idprofil_1 = profil.idprofil) WHERE `bloquer`.`idprofil` = " . $_SESSION["idProfil"] . ";");
    $list = array();
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {

        $list[$i++] = array(
            "id" => $row["idProfil"],
            "username" => $row["nomProfil"],
            "image" => $row["photoProfil"],
        );

    }
    return $list;


}

function getJoursConsecutifs($id)
{

    global $bdd;
    @session_start();


    $result = mysqli_query($bdd, "SELECT * FROM `usersession` WHERE `idprofil` = '" . $id . "'");


    if ($result->num_rows) {
        $tab = mysqli_fetch_array($result);
    } else {
        return 0;
    }

    return $tab["joursConsecutifs"];

}


function getBonusBoost($id)
{

    $joursConsecutifs = getJoursConsecutifs($id);

    if ($joursConsecutifs <= 1) {
        $bonusBoost = 1;
    } elseif ($joursConsecutifs = 2) {
        $bonusBoost = 1.2;
    } elseif ($joursConsecutifs = 3) {
        $bonusBoost = 1.4;
    } elseif ($joursConsecutifs = 4) {
        $bonusBoost = 1.6;
    } elseif ($joursConsecutifs = 5) {
        $bonusBoost = 1.8;
    } elseif ($joursConsecutifs >= 6) {
        $bonusBoost = 2;
    } else {
        $bonusBoost = 1; // au pire des cas... même si ça devrai jamais arriver
    }

    return $bonusBoost;
}


//social

function doILikeThis($idMessage)
{

    global $bdd;
    @session_start();

    $result = mysqli_query($bdd, "SELECT * FROM liker WHERE idProfil = " . $_SESSION["idProfil"] . " AND idMessage = " . $idMessage . ";");
    if ($result->num_rows) {
        return true;
    }
    return false;

}


// message

function getUnreadMessages()
{

    global $bdd;
    @session_start();

    $result = mysqli_query($bdd, "SELECT count(*) as nbMessages FROM chat_msg WHERE idProfil_recepteur = " . $_SESSION["idProfil"] . " AND lu = 0;");
    return mysqli_fetch_array($result)["nbMessages"];

}

function calculThemeLevel($idProfil, $idTheme)
{

    global $bdd;

    $answer = array();

    $result = mysqli_query($bdd, "SELECT SUM(r.points) as sommePoints FROM theme LEFT JOIN question q on theme.idTheme = q.idTheme LEFT JOIN repondre r on q.idQuestion = r.idQuestion WHERE theme.idTheme = " . $idTheme . " AND r.idProfil = " . $idProfil . ";");

    $row = mysqli_fetch_array($result)["sommePoints"];

    $total = (($row + 70) / 270);
    $level = (int)(($row + 70) / 270);
    $progression = -($level - (($row + 70) / 270)) * 100;

    $answer["level"] = $level;
    $answer["total"] = $total;
    $answer["progression"] = $progression;


    return $answer;


}


function insertIntoRemporter($idTitre)
{

    @session_start();
    global $bdd;
    $idProfil = $_SESSION["idProfil"];

    mysqli_query($bdd, "INSERT INTO remporter VALUES (" . $idProfil . ", " . $idTitre . ");");

}


function getAllTitreFromIdThemeAndLevel($arrTitre, $arrRemporter, $userLevel)
{


    $remporter = array(
        "nbTitre" => 0,
        "titres" => array(),
    );

    $dejaEu = array();

    while ($row = mysqli_fetch_array($arrRemporter)) {

        array_push($dejaEu, $row["idTitre"]);

    }
    while ($row = mysqli_fetch_array($arrTitre)) {

        if (!in_array($row["idTitre"], $dejaEu) && $userLevel >= $row["niveauRequis"]) {

            array_push($remporter["titres"], $row["libelleTitre"]);
            $remporter["nbTitre"]++;
            insertIntoRemporter($row["idTitre"]);


        }

    }

    return $remporter;

}

function isThereAnyTitleWon($idProfil, $idTheme)
{

    @session_start();
    global $bdd;

    $answer = array();
    $answer["titreGagne"] = false;
    $answer["listTitre"] = array();

    $titres = mysqli_query($bdd, "SELECT * FROM titre WHERE idTheme = " . $idTheme . ";");

    $remporter = mysqli_query($bdd, "SELECT * FROM remporter WHERE idProfil = " . $idProfil . ";");


    $userLevel = calculThemeLevel($idProfil, $idTheme);


    $result = getAllTitreFromIdThemeAndLevel($titres, $remporter, $userLevel["level"]);


    if ($result["nbTitre"] > 0) {
        $answer["titreGagne"] = true;
        $answer["listTitre"] = $result["titres"];

    }
    $_SESSION["game"]["fromGame"] = false;
    return $answer;


}

function getAllWonTitlesByUserId($id)
{

    global $bdd;

    $result = mysqli_query($bdd, "SELECT * FROM remporter LEFT JOIN titre t on remporter.idTitre = t.idTitre LEFT JOIN theme t2 on t.idTheme = t2.idTheme LEFT JOIN categorie c on t2.idCategorie = c.idCategorie WHERE remporter.idProfil = " . $id . " ORDER BY c.idCategorie, t2.idTheme, t.niveauRequis");

    $userTitre = mysqli_query($bdd, "SELECT * FROM profil WHERE idProfil = " . $id . ";");
    $titreId = mysqli_fetch_array($userTitre)["idTitre"];

    $listeTitre = array();
    $i = 0;

    if ($result->num_rows) {

        while ($row = mysqli_fetch_array($result)) {

            $listeTitre[$i]["id"] = $row["idTitre"];
            $listeTitre[$i]["categorie"] = $row["libelleCategorie"];
            $listeTitre[$i]["theme"] = $row["libelleTheme"];
            $listeTitre[$i]["titre"] = $row["libelleTitre"];
            $listeTitre[$i]["level"] = $row["niveauRequis"];
            if ($titreId == $row["idTitre"]) {


                $listeTitre[$i]["selected"] = true;

            } else {


                $listeTitre[$i]["selected"] = false;

            }

            $i++;
        }

    } else {


        $listeTitre[$i]["id"] = 0;
        $listeTitre[$i]["categorie"] = "Aucun titre !";
        $listeTitre[$i]["theme"] = "";
        $listeTitre[$i]["titre"] = "";
        $listeTitre[$i]["level"] = "";
        $listeTitre[$i]["selected"] = true;

    }

    return $listeTitre;


}

/*
 * Code de samuel Heindricks
 * Je n'ai rien fait personellement de
 * cette fonction.
 *
 * Ne pas côter la fonction. Merci
 * */

function proposeRelatedUser($profil)
{

    global $bdd;

    $sql = "SELECT *
FROM profil
WHERE idProfil NOT IN
      (SELECT idProfil
        FROM bloquer
          WHERE idProfil_1 = '$profil' )
AND idProfil NOT IN
    (SELECT  idProfil_1
        FROM bloquer
        WHERE idProfil = '$profil')
AND idProfil NOT IN
    (SELECT idProfil_1
        FROM s_abonner
        WHERE idProfil = '$profil')
AND idProfil IN 
    (SELECT idProfil
        FROM usersession)
AND idProfil <> '$profil'
ORDER BY RAND()
LIMIT 3 ;";

    $query = mysqli_query($bdd, $sql);
    if ($query->num_rows > 0) {
        $i = 0;
        while ($proposition = mysqli_fetch_array($query)) {
            $affichage[$i]["id"] = $proposition["idProfil"];
            if(!$proposition["photoProfil"]){

                $affichage[$i]["pic"] = "/img/noneProfile.png";
            }else{

                $affichage[$i]["pic"] = $proposition["photoProfil"];
            }
            $affichage[$i]["username"] = $proposition["nomProfil"];
            $i++;
        }
    }
    else
    {

        $affichage[0]["id"] = "";
        $affichage[0]["pic"] = "Erreur";
        $affichage[0]["username"] = "Erreur";

    }

    return $affichage;
}

