<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 14:11
 */

@session_start();

require_once(__DIR__ . "/functions/validator.php");
require_once(__DIR__ . "/functions/json.php");
require_once(__DIR__ . "/functions/user.php");
require_once(__DIR__ . "/database/connect.php");

$champs = array( // ajouter/supprimer les entrees en fonction des champs du formulaire
    "email",
    "username",
    "password",
    "passwordc",
    "lang",
);

$success = true;

$input = array();

$answer = array(
    "success" => false,
    "message" => ""
);

if (isLoggedIn()) {

    $answer["message"] = "<div class='alert alert-danger'><strong>Oups !</strong> Vous êtes déja connecté !</div>";
    $answer["success"] = false;
    encodeAndSendJson($answer);
    die();

}


/*
 *
 * Vérification des champs (regex)
 *
 *
 * */


$errors = "<div class='alert alert-danger'><strong>Oups !</strong> Une ou plusieurs erreurs se sont produite.";
$i = 0;
foreach ($champs as $champ) {
    if (isset($_POST[$champ]) or $_POST[$champ] != "") {
        if (!checkInput($_POST[$champ], $champ)) {
            $success = false;
            $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est invalide.";
            $input[$champ] = "";
        } else {
            $input[$champ] = htmlspecialchars(addslashes($_POST[$champ]));
        }
    } else {
        $success = false;
        $errors .= "<br />" . ++$i . " -  Le champ '" . $champ . "' est manquant. (ajax ?).";
    }

}

$answer["message"] = $errors;


if ($input["password"] != $input["passwordc"]) {
    $answer["message"] .= "<br />" . ++$i . " Les deux mots de passes ne correspondent pas.";
    $success = false;
}
if ($input["lang"] == "") {

    $answer["message"] .= "<br />" . ++$i . " - Veuillez choisir une langue.";
    $success = false;
}

if ($success) {

    $result = mysqli_query($bdd, "SELECT count(*) AS nAccount FROM `usersession` WHERE `email` = '" . $input["email"] . "'");
    $tab = $result->fetch_array();

    if ($tab["nAccount"] > 0) {
        $answer["message"] .= "<br />Cet email existe déjà. Veuillez en choisir un autre.";
        $success = false;
    }

    $result = mysqli_query($bdd, "SELECT count(*) AS nAccount FROM `profil` WHERE `nomProfil` = '" . $input["username"] . "'");
    $tab = $result->fetch_array();

    if ($tab["nAccount"] > 0) {
        $answer["message"] .= "<br />Ce nom d'utilisateur existe déjà. Veuillez en choisir un autre.";
        $success = false;
    }


}


$errors .= "</div>";
$answer["message"] = $errors;

if ($success) {


    $resultProfil = mysqli_query($bdd, "INSERT INTO `profil` (idProfil, nomProfil, photoProfil, photoFacade, villeOrigine, langue, bio, profilPrive, diamants, idTitre, idPays, idRegion) VALUES  
                                                                   (NULL, '" . $input["username"] . "', NULL, NULL, NULL, '" . $input["lang"] . "', NULL, NULL, 50, NULL, NULL, NULL)");

    if (!$resultProfil) {

        $answer["message"] .= "<br />Une erreur s'est produite lors de l'inscription.";
        $success = false;

    }

    $maxNumber = mysqli_query($bdd, "SELECT max(idProfil) as maxId FROM `profil`");
    $max = $maxNumber->fetch_array();

    if ($success) {

        $hashPass = hash("sha256", $input["password"]);
        $resultSession = mysqli_query($bdd, "INSERT INTO `usersession` (email, password, lastActivity, joursConsecutifs, idProfil) VALUES 
                                                                            ('" . $input["email"] . "', '" . $hashPass . "', '" . date("Y/m/d H:i:s", time()) . "', 0, " . $max["maxId"] . ")");

        if ($resultSession) {

            $success = true;
            $answer["success"] = true;
            $answer["message"] = "<div class='alert alert-success'><strong>Bravo !</strong> Inscription reussie ! Redirection...</div>";
            $_SESSION["idProfil"] = $max["maxId"];
        }
    }

}

encodeAndSendJson($answer);