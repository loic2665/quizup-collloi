<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:47
 */
@session_start();
require_once(__DIR__ . "/../php/functions/user.php");
?>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/">QuizUp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/game/index.php">Catégories</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/social/feed.php">Social</a>
            </li>

            <li class="nav-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
            <li class="nav-item">
                <form class="form-inline my-2 my-lg-0"  method="get" action="/social/search.php">
                    <input class="form-control mr-sm-2" type="search" name="recherche" placeholder="Rechercher..."
                           aria-label="Search">
                    <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Rechercher</button>
                </form>
            </li>

        </ul>
        <ul class="navbar-nav">
            <?php if (!isLoggedIn()) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="/login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register.php">S'inscrire</a>
                </li>
            <?php }else{ ?>
                <li class="nav-item">
                    <a class="nav-link" href="/game/listGame.php">
                        Liste des parties
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/messagerie/messages.php">
                        Messagerie
                        <span class="badge badge-pill badge-warning"><?php  echo(getUnreadMessages()); ?></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/social/profile.php">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/php/logout.php">Se déconnecter</a>
                </li>
            <?php } ?>
        </ul>


    </div>
</nav>
