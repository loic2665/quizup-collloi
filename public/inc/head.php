<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:23
 */

require_once(__DIR__."/../php/functions/ajax.php");
require_once(__DIR__."/../php/functions/form.php");
require_once(__DIR__."/../php/functions/themes.php");

?>


<!-- Scripts -->

<script src="/js/jquery-3.3.1.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

<script>
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
</script>

<!-- CSS -->

<meta charset="UTF-8">
<link rel="stylesheet" href="../style.css" type="text/css">
<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/style.css" type="text/css">