<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */


require_once(__DIR__."/../../php/functions/user.php");

redirectIfnotLoggedIn();

?>

<html>
<head>
    <title>Ajouter une catégorie</title>
    <?php require(__DIR__."/../../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/../../inc/nav.php"); ?>

<section>

    <header>
        <h2>Ajouter une catégorie</h2>
    </header>

    <article>

        <div id="serverAnswer">

        </div>

        <div class="form-group">
            <label class="col-form-label col-form-label-lg" for="nomCategorie">Nom de la nouvelle catégorie</label>
            <input class="form-control form-control-lg" type="text" placeholder="Nom de la nouvelle catégorie" id="nomCategorie">
        </div>
        <a class="btn btn-success addCategorie">Ajouter</a>

    </article>

    <script>

        $(".addCategorie").click(function () {
            $.post("./ajax/addCategorie.php",
                {
                    nom: $('#nomCategorie').val(),
                },
                function (data, status) {

                    data = JSON.parse(data);
                    if(data["status"] === true){
                        document.getElementById("serverAnswer").innerHTML = data["message"];
                    }

                });
        });

    </script>
</section>


<?php include(__DIR__."/../../inc/footer.php"); ?>

</body>
</html>
