<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */


require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/themes.php");

if(!isset($_GET["idTheme"])){
    header("Location: /game/");
    die();
}

$idTheme = htmlspecialchars(addslashes($_GET["idTheme"]));

if(!doesThisThemeExist($idTheme)){
    header("Location: /game/");
    die();
}


@session_start();

redirectIfnotLoggedIn();

?>

<html>
<head>
    <title>Ajouter un thème</title>
    <?php require(__DIR__."/../../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/../../inc/nav.php"); ?>

<section>

    <header>
        <h2>Ajouter une question</h2>
    </header>

    <article>

        <div id="serverAnswer">
            <?php echo(@$_SESSION["addTitre"]); $_SESSION["addTitre"] = ""; ?>
        </div>


        <form action="./ajax/addTitre.php" method="post">


            <div class="form-group">
                <label class="col-form-label col-form-label" for="libelle">Libellé du nouveau titre</label>
                <input class="form-control form-control" type="text" placeholder="Libellé du nouveau titre" id="libelle" name="libelle">
            </div>

            <div class="form-group">
                <label class="col-form-label col-form-label" for="niveau">Niveau requis du nouveau titre</label>
                <input class="form-control form-control" type="number" min="1" placeholder="Niveau requis du nouveau titre" id="niveau" name="niveau">
            </div>


            <input name="idTheme" hidden  value="<?php echo($_GET["idTheme"]); ?>" />


            <input type="submit" class="btn btn-success addTheme" value="Ajouter">

        </form>

    </article>


</section>


<?php include(__DIR__."/../../inc/footer.php"); ?>

</body>
</html>
