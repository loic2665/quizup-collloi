<?php


require_once(__DIR__ . "/../../../php/database/connect.php");
require_once(__DIR__ . "/../../../php/functions/user.php");
require_once(__DIR__ . "/../../../php/functions/json.php");
require_once(__DIR__ . "/../../../php/functions/themes.php");

if (!isLoggedIn()) {
    die();
}

$idTheme = htmlspecialchars(addslashes($_POST["idTheme"]));

@session_start();

$_SESSION["addTheme"] = array();

if (!isset($_POST["question"]) || !isset($_POST["goodAnswer"]) || !isset($_POST["badAnswer1"]) || !isset($_POST["badAnswer2"]) || !isset($_POST["badAnswer3"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Requête incorecte !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}

if (empty($_POST["question"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Question vide !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}

if (empty($_POST["goodAnswer"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Bonne réponse vide !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}


if (empty($_POST["badAnswer1"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Mauvaise réponse 1 vide !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}


if (empty($_POST["badAnswer2"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Mauvaise réponse 2 vide !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}


if (empty($_POST["badAnswer3"])) {

    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Mauvaise réponse 3 vide !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}

$question = addslashes($_POST["question"]);
$goodAnswer = addslashes($_POST["goodAnswer"]);
$badAnswer1 = addslashes($_POST["badAnswer1"]);
$badAnswer2 = addslashes($_POST["badAnswer2"]);
$badAnswer3 = addslashes($_POST["badAnswer3"]);
$now = date("Y-m-d H:i:s", time());


if (!doesThisThemeExist($idTheme)) {
    header("Location: /game/");
    die();
}

if(!doesThisThemeBelongToThisUser($idTheme)){


    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Le thème ne vous appartient pas !</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();


}

// IMAGE IMAGE IMAGE
$allOK = 1;
if (isset($_POST["isImage"]) && $_POST["isImage"] == "on") {
    $target_dir = __DIR__ . "/../../../img/questions/"; // dossier de l'upload

    $target_file = $target_dir . basename($_FILES["imgQuestion"]["name"]); // dossier upload + filename
    $idunique = uniqid();
    $target_file_to_upload = $target_dir . $idunique; // dossier upload + timestamp unique


    $uploadOk = 0; // on assume que l'image est pas correcte
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image


    $check = getimagesize($_FILES["imgQuestion"]["tmp_name"]);


    if ($check !== false) {

        $uploadOk = 1;

    } else {

        $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée en n'est pas une... sois sympa...</div>";
        header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
        die();


    }


// Check if file already exists
    if (file_exists($target_file)) {
        unlink($target_file);
    }
// Check file size
    if ($_FILES["imgQuestion"]["size"] > 500000) { // pas plus de 500ko...
        $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée est trop grosse... (max. 500 Ko)</div>";
        header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
        die();
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {


        $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Les extensions de fichier autorisée sont PNG, JP(E)G, GIF.</div>";
        header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
        die();

    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 1) {

        if (move_uploaded_file($_FILES["imgQuestion"]["tmp_name"], $target_file_to_upload . "." . $imageFileType)) {
            $allOK = 1;


            $url = $target_file_to_upload . "." . $imageFileType;
            $url = explode("game/tools/ajax/../../..", $url)[1];


            // todo : a ameliorer !!


        } else {

            $allOK = 0;
            $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Image non conforme !</div>";
            header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");

            die();
        }


    }

} else {

    $url = "";

}

// FIN IMG FIN IMG FIN IMG

if ($allOK) {

    if ($url == "") {
        mysqli_query($bdd, "INSERT INTO question VALUES (NULL, NULL, '" . $question . "', '" . $goodAnswer . "', '" . $badAnswer1 . "', '" . $badAnswer2 . "', '" . $badAnswer3 . "', " . $idTheme . ")");

    } else {
        mysqli_query($bdd, "INSERT INTO question VALUES (NULL, '" . $url . "', '" . $question . "', '" . $goodAnswer . "', '" . $badAnswer1 . "', '" . $badAnswer2 . "', '" . $badAnswer3 . "', " . $idTheme . ")");

    }


    mysqli_query($bdd, "UPDATE theme SET dateUpdated = '" . date("Y-m-d H:i:s", time()) . "' WHERE idTheme = ".$idTheme."");

    $_SESSION["addQuestion"] = "<div class='alert alert-success'><strong>Yaas !</strong> Question ajoutée !</div>";


    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();

} else {
    $_SESSION["addQuestion"] = "<div class='alert alert-primary'><strong>Oops !</strong> Une erreur s'est produite lors de l'envoi !!</div>";
    header("Location: ../addQuestion.php?idTheme=" . $idTheme . "");
    die();
}