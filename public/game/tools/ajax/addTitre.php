<?php


require_once(__DIR__ . "/../../../php/database/connect.php");
require_once(__DIR__ . "/../../../php/functions/user.php");
require_once(__DIR__ . "/../../../php/functions/json.php");
require_once(__DIR__ . "/../../../php/functions/themes.php");

if (!isLoggedIn()) {
    die();
}


$idTheme = htmlspecialchars(addslashes($_POST["idTheme"]));


@session_start();


if(!isset($_POST["libelle"]) || empty($_POST["libelle"])) {

    $_SESSION["addTitre"] = "<div class='alert alert-primary'><strong>Oops !</strong> Libellé vide !</div>";
    header("Location: ../addTitre.php?idTheme=" . $idTheme . "");
    die();

}
if(!isset($_POST["niveau"]) || empty($_POST["niveau"])) {

    $_SESSION["addTitre"] = "<div class='alert alert-primary'><strong>Oops !</strong> Niveau requis vide !</div>";
    header("Location: ../addTitre.php?idTheme=" . $idTheme . "");
    die();

}

$libelle = htmlspecialchars(addslashes($_POST["libelle"]));
$niveau = htmlspecialchars(addslashes($_POST["niveau"]));


$result = mysqli_query($bdd, "SELECT * FROM titre WHERE idTheme = ".$idTheme." AND libelleTitre like '".$libelle."'");

if($result->num_rows){

    $_SESSION["addTitre"] = "<div class='alert alert-primary'><strong>Oops !</strong> Libellé déja existant !</div>";
    header("Location: ../addTitre.php?idTheme=" . $idTheme . "");
    die();

}

$result = mysqli_query($bdd, "INSERT INTO titre VALUES (NULL, '".$libelle."', '".$niveau."', '".$idTheme."');");

if(!$result){

    $_SESSION["addTitre"] = "<div class='alert alert-primary'><strong>Oops !</strong> Echec de l'ajout ! </div>";
    header("Location: ../addTitre.php?idTheme=" . $idTheme . "");
    die();

}else{

    $_SESSION["addTitre"] = "<div class='alert alert-success'><strong>Yes !</strong> Ajout réussi !</div>";
    header("Location: ../addTitre.php?idTheme=" . $idTheme . "");
    die();

}


?>