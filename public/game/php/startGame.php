<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 24/03/19
 * Time: 15:09
 */




require_once(__DIR__ . "/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/game.php");
redirectIfnotLoggedIn();

@session_start();

if (!isset($_GET["idTheme"])) {
    header("Location: /game/");
    die();
}

$idTheme = addslashes(htmlspecialchars($_GET["idTheme"]));
require_once(__DIR__ . "/../../php/functions/themes.php");
$theme = getThemeDetailsById($idTheme);

if (!$theme["success"]) {
    header("Location: /game/");
    die();
}

$status = initGame($theme["details"]["id"]);

if($status){
    header("Location: /game/play.php");
    die();
}else{
    header("Location: /game/");
    die();
}