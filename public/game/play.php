<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 13:01
 */


require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/themes.php");
redirectIfnotLoggedIn();


@session_start();

//echo(json_encode($_SESSION));

$nQuestion = $_SESSION["game"]["numQuestion"];
$theme = getCategorieByThemeId($_SESSION["game"]["idTheme"]);

$quesionData = array(

    "categorie" => array(
        "id" => $theme["details"]["id"],
        "nom" => $theme["details"]["nom"],
    ),
    "question" => $_SESSION["game"]["content"][$nQuestion]["question"],
    "answers" => array(
        0 => $_SESSION["game"]["content"][$nQuestion]["answers"][0]["content"],
        1 => $_SESSION["game"]["content"][$nQuestion]["answers"][1]["content"],
        2 => $_SESSION["game"]["content"][$nQuestion]["answers"][2]["content"],
        3 => $_SESSION["game"]["content"][$nQuestion]["answers"][3]["content"],
    ),
    "image" => $_SESSION["game"]["content"][$nQuestion]["image"]

);


?>

<html>
<head>
    <title>Jeu</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>


</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>


<section>
    <header>
        <h2>Jeu</h2>
    </header>

    <article class="gameCard">


        <div class="card mb-3">
            <h3 class="card-header">Question <span id="nQuestion"><?php echo($nQuestion + 1); ?></span>/7 - <span id="totalPoints"><?php echo($_SESSION["game"]["totalPoints"]); ?></span> points</h3>
            <div class="card-body">
                <p>
                    Temps
                </p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                         id="timerQuestion" style="width: 100%"></div>
                </div>
                <br/>
                <h5 class="card-title"><?php echo($quesionData["categorie"]["nom"]); ?></h5>
                <h6 class="card-subtitle text-muted"><?php echo($_SESSION["game"]["nomTheme"]); ?></h6>
            </div>

                <img style="height: 200px; width: 30%; display: <?php if($quesionData["image"]){ ?>block<?php }else{?>none<?php } ?>; margin: auto"
                     src="<?php echo($quesionData["image"]); ?>" id="imgQuestion" alt="Card image">

            <div class="card-body">
                <p class="card-text" id="textQuestion"><?php echo($quesionData["question"]); ?></p>
            </div>
            <ul class="list-group list-group-flush">
                <?php
                $i = 0;
                foreach ($quesionData["answers"] as $answer) {
                    ?>
                    <li class="list-group-item">
                        <button type="button" id="answer<?php echo($i); ?>"
                                class="btn btn-danger btn-lg btn-block answer<?php echo($i); ?>"
                                value="<?php echo($i); ?>"><?php echo($answer); ?></button>
                        <script>

                            $(".answer<?php echo($i); ?>").click(function () {
                                $.post("/game/ajax/answerQuest.php",
                                    {
                                        idRep: <?php echo($i); ?>,
                                    },
                                    function (data, status) {


                                        data = JSON.parse(data);
                                        console.log(data);

                                        disableAllButton();

                                        if (data["details"]["isValid"] === true) {
                                            document.getElementById("answer<?php echo($i); ?>").style.backgroundColor = "#22B24C";
                                            document.getElementById("answer<?php echo($i); ?>").style.borderColor = "#22B24C";
                                            document.getElementById("totalPoints").innerText = data["details"]["totalPoints"];
                                            console.log(data["details"]["points"]);
                                            playGoodAnswer();
                                        } else {
                                            document.getElementById("answer<?php echo($i); ?>").style.backgroundColor = "#890B07";
                                            document.getElementById("answer<?php echo($i); ?>").style.borderColor = "#890B07";
                                            playBadAnswer();
                                        }

                                        if (data["endGame"] === true) {
                                            console.log("END");
                                            disableAllButton();
                                            resetTimer();
                                            endGame();
                                            sleep(3500).then(() => {
                                                // Do something after the sleep!
                                                var idPartie = data["newIdPartie"];
                                                window.location.replace("/game/result.php?idPartie=" + idPartie);
                                            });


                                        } else {


                                            sleep(1000).then(() => {

                                                timer();
                                                resetButtonColor();
                                                enableAllButton();
                                                nextQuestion();
                                                for (var l = 0; l < 4; l++) {
                                                    document.getElementById("answer" + l).innerText = data["answer"]["nextQuestion"][l]["content"];
                                                }
                                                if(data["isImage"] === true){
                                                    document.getElementById("imgQuestion").setAttribute("src", data["imgQuestion"]);
                                                    document.getElementById("imgQuestion").style.display = "block";
                                                }else{

                                                    document.getElementById("imgQuestion").style.display = "none";
                                                }

                                                document.getElementById("nQuestion").innerText = data["answer"]["idQuestion"];
                                                document.getElementById("textQuestion").innerText = data["textQuestion"];
                                            });
                                            resetTimer();
                                        }



                                    });

                            });

                        </script>
                    </li>
                    <?php
                    $i++;
                }
                ?>

            </ul>


        </div>


    </article>

</section>

<?php include(__DIR__ . "/../inc/footer.php"); ?>


<script>
    <?php if($_SESSION["game"]["numQuestion"] == 0){ ?>
    var startGame      = new Audio('/game/assets/sound/startGame.ogg');
    startGame.play();
    <?php } ?>
    var theme      = new Audio('/game/assets/music/theme.ogg');
    theme.play();

    function playGoodAnswer(){

        var goodAnswer = new Audio('/game/assets/sound/goodAnswer.ogg');
        goodAnswer.play();
    }

    function playBadAnswer() {


        var badAnswer  = new Audio('/game/assets/sound/badAnswer.ogg');
        badAnswer.play();

    }

    function nextQuestion() {

        var nextQuestion  = new Audio('/game/assets/sound/nextQuestionGame.ogg');
        nextQuestion.play();

    }

    function disableAllButton() {
        for (var i = 0; i < 4; i++) {
            document.getElementById("answer" + i).disabled = true;
        }
    }

    function endGame(){
        var endGame      = new Audio('/game/assets/sound/endGame2.ogg');
        window.theme.pause();
        endGame.play();
    }

    function enableAllButton() {
        for (var i = 0; i < 4; i++) {
            document.getElementById("answer" + i).disabled = false;
        }
    }

    function resetButtonColor() {
        for (var i = 0; i < 4; i++) {
            document.getElementById("answer" + i).style.backgroundColor = "#F57A00";
            document.getElementById("answer" + i).style.borderColor = "#F57A00";
        }

    }


    function resetTimer() {

        $('#timerQuestion').animate({"width": '100%'}, 0, "linear");
        $('#timerQuestion').stop();
    }

    function timer() {
        $('#timerQuestion').animate({"width": 0}, 7500, "linear");
    }

    timer();

</script>

</body>
</html>

