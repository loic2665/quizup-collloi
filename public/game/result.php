<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 1/04/19
 * Time: 20:03
 */


require_once(__DIR__ . "/../php/functions/themes.php");
require_once(__DIR__ . "/../php/functions/game.php");

@redirectIfnotLoggedIn();

if (!isset($_GET["idPartie"])) {
    header("Location: /game/");
    die();
}

$idPartie = addslashes(htmlspecialchars($_GET["idPartie"]));

if (!doesThisPartieExist($idPartie) || empty($_GET["idPartie"])) {
    header("Location: /game/");
    die();
}

?>

<html>
<head>
    <title>Résultats</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>

<section>
    <header>
        <h2>Résultat de la partie n°<?php echo($idPartie); ?></h2>
    </header>

    <article>

        <?php
        $result = getAllQuestionsFromPartieId($idPartie);

        ?>

        <div class="row">


            <div class="col-md-3">
                <div class="card mb-3">
                    <h3 class="card-header">Résultats</h3>
                    <div class="card-body">

                        <h3>Score :</h3>
                        <h5 class="card-title"><?php echo($result["totalPoints"]); ?> / 130 points
                            <?php if ($result["totalPoints"] > 130) { ?>
                                <img alt="perfect score" src="/img/perfect32x.png"/>
                            <?php } ?>

                        </h5>

                        <h3>Date de la partie :</h3>
                        <p><?php echo(explode(" ", $result["datePartie"])[0]); ?> à
                            <?php echo(explode(" ", $result["datePartie"])[1]); ?></p>

                        <h3>
                            Catégorie :
                        </h3>
                        <p>
                            <?php $cate = getCategorieByThemeId($result["themeData"]["details"]["id"]);
                            echo($cate["details"]["nom"]); ?>
                        </p>


                        <h3>
                            Thème :
                        </h3>
                        <p>
                            <?php echo($result["themeData"]["details"]["nom"]); ?>
                        </p>


                        <?php if (isPersonnalProfile($result["idProfil"])) { ?>

                            <?php $level = calculThemeLevel($_SESSION["idProfil"], $result["themeData"]["details"]["id"]); ?>


                            <h3>
                                Niveau <?php echo($level["level"]); ?>
                            </h3>
                            <p>
                                <?php echo((int)$level["progression"]); ?> /100
                            </p>
                            <div class="progress">
                                <div id="levelBar" class="progress-bar progress-bar-striped progress-bar-animated"
                                     role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                            </div>

                            <?php

                            if (@$_SESSION["game"]["fromGame"]) {
                                $titres = isThereAnyTitleWon($_SESSION["idProfil"], $result["themeData"]["details"]["id"]);

                                if ($titres["titreGagne"]) {


                                    ?>

                                    <h3>
                                        Titre gagnés
                                    </h3>

                                    <?php

                                    $textToShow = "";

                                    foreach ($titres["listTitre"] as $titre) {
                                        $textToShow .= $titre . ", ";
                                    }


                                    ?>

                                    <p>
                                        <?php echo(substr($textToShow, 0, strlen($textToShow) - 2) . "."); ?>
                                    </p>

                                    <?php
                                }
                            }

                        }
                        ?>
                    </div>


                </div>

            </div>


            <?php for ($i = 0; $i < 7; $i++) { ?>

                <div class="col-md-3">
                    <div class="card mb-3">
                        <h3 class="card-header">Question <?php echo($i + 1); ?>/7</h3>
                        <div class="card-body">
                            <h5 class="card-title"><?php if ($result["questions"][$i]["reussi"]) { ?>Bonne réponse !<?php } else { ?>Mauvaise réponse...<?php } ?></h5>
                            <h6 class="card-subtitle text-muted">+<?php echo($result["questions"][$i]["points"]); ?>
                                point(s) !</h6>
                        </div>
                        <?php if ($result["questions"][$i]["image"]) { ?>
                            <img style="height: 200px; width: 100%;"
                                 src="<?php echo($result["questions"][$i]["image"]); ?>"
                                 alt="Card image">
                        <?php } ?>

                        <div class="card-body">
                            <p class="card-text"><?php echo($result["questions"][$i]["textQuestion"]); ?></p>
                        </div>
                        <ul class="list-group list-group-flush">


                            <?php foreach ($result["questions"][$i]["answers"] as $answer) { ?>
                                <li class="list-group-item">
                                    <?php
                                    $class = "btn-danger";
                                    if ($answer["valid"] && $answer["selected"]) {
                                        $class = "btn-success";
                                    } else if (!$answer["valid"] && $answer["selected"]) {
                                        $class = "btn-primary";
                                    } else if ($answer["valid"] && !$answer["selected"]) {
                                        $class = "btn-success";
                                    } else {
                                        $class = "btn-danger";
                                    }

                                    ?>

                                    <button type="button" class="btn <?php echo($class); ?> btn-lg btn-block">
                                        <?php echo($answer["content"]); ?>
                                    </button>
                                </li>
                            <?php } ?>

                        </ul>


                    </div>

                </div>


            <?php } ?>


        </div>
    </article>

</section>


<section>

    <header>
        <h2>Connaissez-vous...</h2>
    </header>

    <article>

        <div class="container">
            <div class="row">

                <?php

                $relatedUser = proposeRelatedUser($_SESSION["idProfil"]);
                foreach ($relatedUser as $user){

                ?>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo($user["username"]); ?></h4>
                            <div>
                                <img src="<?php echo($user["pic"]); ?>" alt="<?php echo($user["username"]); ?>" class="profilePic relatedUser">
                            </div>
                            <p>
                                <br />
                            <a href="/social/profile.php?idProfil=<?php echo($user["id"]); ?>" class="btn btn-info">Voir le profil</a>
                            </p>
                        </div>
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </article>

</section>

<?php if (!isset($_GET["fromList"]) && !@$_GET["fromList"]) { ?>
    <script>

        var recapGame = new Audio('/game/assets/sound/recap.ogg');
        recapGame.play();

    </script>

<?php } ?>
<script>
    function toLevel() {

        $('#levelBar').animate({"width": '<?php echo((int)$level["progression"]); ?>%'}, 750, "linear");
    }

    toLevel();


</script>

<?php include("../inc/footer.php"); ?>

</body>
</html>

