<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 15:13
 */

require_once(__DIR__."/../../php/database/connect.php");
require_once(__DIR__."/../../php/functions/json.php");
require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/themes.php");

@session_start();

$answer = array();

if(!isLoggedIn()){
    die();
}

if(!isset($_POST["idTheme"]) || empty($_POST["idTheme"])){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Requête incorrecte.</div>';
    die(encodeAndSendJson($answer));


}

$idTheme = addslashes(htmlspecialchars($_POST["idTheme"]));


$result = mysqli_query($bdd, "SELECT * FROM theme WHERE idTheme = ".$idTheme.";");


if($result->num_rows == 0){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Ce thème n\'existe pas.</div>';
    die(encodeAndSendJson($answer));

}


$result = mysqli_query($bdd, "SELECT * FROM suivre WHERE idProfil = ".$_SESSION["idProfil"]." AND idTheme = ".$idTheme.";");


if($result->num_rows == 0){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu ne suivais déja pas le thème.</div>';
    die(encodeAndSendJson($answer));

}

$result = mysqli_query($bdd, "DELETE FROM suivre WHERE idProfil = ".$_SESSION["idProfil"]." AND idTheme = ".$idTheme.";");

if(!mysqli_affected_rows($bdd)){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Une erreur s\'est produite.</div>';
    die(encodeAndSendJson($answer));

}else{

    $answer["success"] = true;
    $answer["message"] = '<div class="alert alert-success"><strong>Yes! </strong> Tu ne suis plus ce thème.</div>';
    die(encodeAndSendJson($answer));

}