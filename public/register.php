<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 *  Remarques : les messages d'erreurs ne sont pas
 *  très explicite, et dans un cas précis, il y a une erreur qui
 *  survient sans message d'erreur...
 *
 *  
 *
 *
 *
 *
 *
 */

require_once(__DIR__ . "/php/functions/user.php");
redirectIfLoggedIn();

?>


<html>
<head>
    <title>Accueil</title>
    <?php require("./inc/head.php"); ?>
</head>
<body>

<?php require("./inc/nav.php"); ?>

<section>

    <div id="serverAnswer">

    </div>

    <?php


    $fields = array(
        array(
            "label" => "Email",
            "type" => "email",
            "id" => "email",
            "placeholder" => "Votre email (e.mail@fournisseur.extension)",

        ),
        array(
            "label" => "Nom d'utilisateur",
            "type" => "text",
            "id" => "username",
            "placeholder" => "Nom d'utilisateur (min. 3 caract)",

        ),
        array(
            "label" => "Mot de passe",
            "type" => "password",
            "id" => "password",
            "placeholder" => "Votre mot de passe (au moins un caractère majuscule, minuscule, chiffre, min 8 caractères)",

        ),
        array(
            "label" => "Confirmez votre mot de passe",
            "type" => "password",
            "id" => "passwordc",
            "placeholder" => "Confirmation de votre mot de passe",

        ),
        array(
            "label" => "Langue",
            "type" => "select",
            "id" => "lang",
            "options" => array(
                "Français",
                "Anglais",
                "Neerlandais",
                "Almemand",
                "Espagnol",
            ),

        ),
    );
    ?>

    <fieldset>
        <legend>Inscription</legend>

        <?php foreach ($fields as $field) { ?>

            <?php if ($field["type"] == "select") { ?>

                <div class="form-group">
                    <label for="<?php echo($field["id"]); ?>"><?php echo($field["label"]); ?></label>
                    <select class="custom-select" id="<?php echo($field["id"]); ?>">
                        <option disabled selected="" value="none">Choisissez une langue</option>
                        <?php foreach ($field["options"] as $option) { ?>
                            <option value="<?php echo($option); ?>">
                                <?php echo($option); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <label for="<?php echo($field["id"]); ?>"><?php echo($field["label"]); ?></label>
                    <input type="<?php echo($field["type"]); ?>" class="form-control" id="<?php echo($field["id"]); ?>"
                           placeholder="<?php echo($field["placeholder"]); ?>">
                </div>
            <?php } ?>
        <?php } ?>


        <button type="submit" class="register btn btn-primary">S'inscrire</button>
    </fieldset>


</section>


<?php include("./inc/footer.php"); ?>


<?php generateAjax("register", $fields, "/php/register.php", 2000, "/social/profile.php"); ?>


</body>
</html>
