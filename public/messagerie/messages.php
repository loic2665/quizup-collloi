<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */
require_once(__DIR__."/../php/functions/user.php");
redirectIfnotLoggedIn()
?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
    <link rel="stylesheet" href="./css/messagerie.css" type="text/css"/>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>

<section>
    <header>
        <h2>Discussion instantanée - Chat avec <span id="nomUser">%user%</span></h2>
    </header>

    <article>


        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="chat-userList">

                    <div class="list-group">

                        <?php

                        $result = mysqli_query($bdd, "SELECT * FROM profil LEFT JOIN usersession u on profil.idProfil = u.idProfil");


                        ?>

                        <?php while ($row = mysqli_fetch_array($result)) { if($row["idProfil"] == $_SESSION["idProfil"]){continue;}


                            $last = mysqli_query($bdd, "SELECT * FROM `chat_msg` WHERE `idProfil_emetteur` = " . $_SESSION["idProfil"] . " AND `idProfil_recepteur` = " . $row["idProfil"] . " OR `idProfil_emetteur` = " . $row["idProfil"] . " AND `idProfil_recepteur` = " . $_SESSION["idProfil"] . " ORDER BY idChatMsg DESC LIMIT 1;");
                            if($last->num_rows){
                                $lastMsg = mysqli_fetch_array($last)["contenu"];
                            }else{
                                $lastMsg = "Aucun message...";
                            }
                        ?>
                            <a onclick="changeUserDestination(<?php echo($row["idProfil"]); ?>)"
                               class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1"><?php echo($row["nomProfil"]); ?></h5>

                                </div>
                                <small class="text-muted"><?php echo(substr($lastMsg, 0, 30)); ?>...</small>
                            </a>
                        <?php } ?>
                    </div>

                </div>

            </div>
            <div class="col-sm-9">

                <div class="chat-messages" id="message-list">

                    <div class="list-group" id="listMessage">



                    </div>

                </div>

            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-3">
                <div class="chat-none" id="statusServer">

                </div>

            </div>
            <div class="col-sm-6">
                <div class="chat-textBox">
                    <textarea class="form-control" id="message"></textarea>
                    <input id="idUser" value="0"/>
                </div>

            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="chat-sendButton">
                    <button class="btn btn-primary envoi">Envoyer</button>
                </div>

            </div>
        </div>


    </article>
</section>




<script>


    function refreshConversation(){
        loadConversation(false);
    }

    window.setInterval(function(){
        /// call your function here
        if(document.getElementById("idUser").value != 0){
            refreshConversation();
        }
    }, 1000);


    function changeUserDestination(value) {

        document.getElementById("idUser").setAttribute("value", value);
        loadConversation(true);

    }

    function scrollToBottom() {

        var objDiv = document.getElementById("message-list");
        objDiv.scrollTop = objDiv.scrollHeight;

    }


    $(".envoi").click(function () {
        $.post("./ajax/sendMessage.php",
            {
                idUser: $('#idUser').val(),
                messageUser: $('#message').val(),
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {

                    loadConversation(false);
                    scrollToBottom();

                }
                document.getElementById("statusServer").innerHTML = data["message"];
                document.getElementById("message").value = "";


            }
        );
    });


    function loadConversation(scroll) {


        $.post("./ajax/loadConversation.php",
            {
                idUser: $('#idUser').val(),
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] == true) {

                    var conversationToShow = "";
                    document.getElementById("nomUser").innerText = data["username"];

                    var i = 0;

                    while(i < data["listMsg"].length){

                        conversationToShow += data["listMsg"][i].replace(/\n/ig, '<br />');;
                        i += 1;

                    }

                    document.getElementById("listMessage").innerHTML = conversationToShow;


                    if(scroll){
                        scrollToBottom();

                    }



                }


            }
        );


    }




</script>


<?php include(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>
