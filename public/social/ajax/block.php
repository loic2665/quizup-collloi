<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 15:13
 */

require_once(__DIR__."/../../php/database/connect.php");
require_once(__DIR__."/../../php/functions/json.php");
require_once(__DIR__."/../../php/functions/user.php");

@session_start();

$answer = array();

if(!isLoggedIn()){
    die();
}

if(!isset($_POST["idUser"]) || empty($_POST["idUser"])){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Requête incorrecte.</div>';
    die(encodeAndSendJson($answer));


}

$idUser = addslashes(htmlspecialchars($_POST["idUser"]));

if($idUser == $_SESSION["idProfil"]){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu ne peux pas te bloquer.</div>';
    die(encodeAndSendJson($answer));


}

$result = mysqli_query($bdd, "SELECT * FROM bloquer WHERE idProfil = ".$_SESSION["idProfil"]." AND idProfil_1 = ".$idUser.";");
mysqli_query($bdd, "DELETE FROM s_abonner WHERE idProfil = ".$_SESSION["idProfil"]." AND idProfil_1 = ".$idUser.";");
mysqli_query($bdd, "DELETE FROM s_abonner WHERE idProfil_1 = ".$_SESSION["idProfil"]." AND idProfil = ".$idUser.";");

if($result->num_rows > 0){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu as déja bloqué cet utilisateur.</div>';
    die(encodeAndSendJson($answer));

}

$result = mysqli_query($bdd, "INSERT INTO bloquer VALUES (".$_SESSION["idProfil"].", ".$idUser.");");

if(!mysqli_affected_rows($bdd)){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Une erreur s\'est produite.</div>';
    die(encodeAndSendJson($answer));

}else{

    $answer["success"] = true;
    $answer["message"] = '<div class="alert alert-success"><strong>Yes! </strong> L\'utilisateur à bien été bloqué.</div>';
    die(encodeAndSendJson($answer));

}