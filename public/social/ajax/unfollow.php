<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 15:13
 */

require_once(__DIR__."/../../php/database/connect.php");
require_once(__DIR__."/../../php/functions/json.php");
require_once(__DIR__."/../../php/functions/user.php");

@session_start();

$answer = array();

if(!isLoggedIn()){
    die();
}

if(!isset($_POST["idUser"]) || empty($_POST["idUser"])){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Requête incorrecte.</div>';
    die(encodeAndSendJson($answer));


}

$idUser = addslashes(htmlspecialchars($_POST["idUser"]));

if($idUser == $_SESSION["idProfil"]){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu ne peux pas ne plus suivre (qui fait ça ?).</div>';
    die(encodeAndSendJson($answer));


}

$result = mysqli_query($bdd, "SELECT * FROM bloquer WHERE idProfil = ".$_SESSION["idProfil"]." AND idProfil_1 = ".$idUser.";");


if($result->num_rows > 0){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu ne peux pas ne plus suivre un utilisateur qui t\'as bloqué... (comment as-tu pu faire ça ?)</div>';
    die(encodeAndSendJson($answer));

}


$result = mysqli_query($bdd, "SELECT * FROM s_abonner WHERE idProfil = ".$_SESSION["idProfil"]." AND idProfil_1 = ".$idUser.";");


if($result->num_rows == 0){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Tu ne suivais pas l\'utilisateur...</div>';
    die(encodeAndSendJson($answer));

}

mysqli_query($bdd, "DELETE FROM s_abonner WHERE idProfil = ".$_SESSION["idProfil"]." AND idProfil_1 = ".$idUser.";");

if(!mysqli_affected_rows($bdd)){

    $answer["success"] = false;
    $answer["message"] = '<div class="alert alert-primary"><strong>Hum... </strong> Une erreur s\'est produite.</div>';
    die(encodeAndSendJson($answer));

}else{

    $answer["success"] = true;
    $answer["message"] = '<div class="alert alert-success"><strong>Yes! </strong> Tu suis ne suis plus cet utiliateur.</div>';
    die(encodeAndSendJson($answer));

}