<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 15:13
 */

require_once(__DIR__ . "/../../php/database/connect.php");
require_once(__DIR__ . "/../../php/functions/json.php");
require_once(__DIR__ . "/../../php/functions/user.php");
require_once(__DIR__ . "/../../php/functions/social.php");
require_once(__DIR__ . "/../../php/functions/themes.php");

@session_start();

$answer = array();

if (!isLoggedIn()) {
    die();
}

if (!isset($_POST["idMessage"]) ||
    empty($_POST["idMessage"]) ||
    !isset($_POST["contenu"]) ||
    empty($_POST["contenu"]) ||
    !isset($_POST["idTheme"]) ||
    empty($_POST["idTheme"])
) {

    $answer["success"] = false;
    $answer["refresh"] = false;
    $answer["alert"] = "Requête incorrecte ! (ajax !)";

    die(encodeAndSendJson($answer));


}

$idMessage = addslashes(htmlspecialchars($_POST["idMessage"]));
$contenu = addslashes(htmlspecialchars($_POST["contenu"]));
$idTheme = addslashes(htmlspecialchars($_POST["idTheme"]));

if (!doesThisThemeExist($idTheme)) {

    $answer["success"] = false;
    $answer["refresh"] = false;
    $answer["alert"] = "Thème inexistant !";

    die(encodeAndSendJson($answer));

}

if ($idMessage != -1) {


    $result = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage = " . $idMessage . ";");

    if ($result->num_rows == 0) {

        $answer["success"] = false;
        $answer["refresh"] = true;
        $answer["alert"] = "Le commentaire n'existe plus, la page va se rafraichir.";

        die(encodeAndSendJson($answer));

    }
    $nivComment = 1;

}else{


    $nivComment = 0;

}


$isComment = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage_1 IS NOT NULL AND idMessage = " . $idMessage . ";");
//$isComment = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage_1 IS NOT NULL AND idMessage = 3;");

if ($isComment->num_rows) {
    $row = mysqli_fetch_array($isComment);
    $nivComment++;

    $isComment = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage_1 IS NOT NULL AND idMessage = " . $row["idMessage_1"] . ";");

    if ($isComment->num_rows) {
        $row = mysqli_fetch_array($isComment);

        $nivComment++;

        $isComment = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage_1 IS NULL AND idMessage = " . $row["idMessage_1"] . ";");

        if ($isComment->num_rows) {

            $nivComment++;

        }


    }


}

if ($nivComment > 2) {


    $answer["success"] = false;
    $answer["refresh"] = false;
    $answer["alert"] = "Vous ne pouvez pas répondre à un commentaire d'un commentaire d'un post.";
    die(encodeAndSendJson($answer));

}




if ($nivComment == 0) {

    $result = mysqli_query($bdd, "INSERT INTO message VALUES (NULL, '" . date("Y/m/d H:i:s", time()) . "', '" . $contenu . "', NULL, " . $idTheme . ", " . $_SESSION["idProfil"] . ");");

} else {
    $message1 = $idMessage;
    $result = mysqli_query($bdd, "INSERT INTO message VALUES (NULL, '" . date("Y/m/d H:i:s", time()) . "', '" . $contenu . "', " . $message1 . ", " . $idTheme . ", " . $_SESSION["idProfil"] . ");");

}

if ($result) {

    $answer["success"] = true;
    $answer["refresh"] = true;
    $answer["alert"] = "Commentaire publié ! La page va se rafraichir.";
    die(encodeAndSendJson($answer));

} else {

    $answer["success"] = false;
    $answer["refresh"] = true;
    $answer["alert"] = "Une erreur s'est produite... Impossible de publier le commentaire." . mysqli_error($bdd);
    die(encodeAndSendJson($answer));

}