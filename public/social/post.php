<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:56
 */
require_once(__DIR__ . "/../php/database/connect.php");
require_once(__DIR__ . "/../php/functions/themes.php");
require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");

redirectIfnotLoggedIn();

if(!isset($_GET["idPost"]) || empty($_GET['idPost'])){
    header("Location: ./feed.php");
    die();
}

$idMessage = addslashes(htmlspecialchars($_GET["idPost"]));

if(!doesThisPostExist($idMessage)){
    header("Location: ./feed.php");
    die();
}

?>

<html>
<head>
    <title>Accueil</title>
    <?php require_once(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require_once(__DIR__ . "/../inc/nav.php"); ?>


<section>
    <header>
        <h2>Commentaire du post</h2>
    </header>

    <article>


        <br />

        <?php


        $result = mysqli_query($bdd, "SELECT * FROM message LEFT JOIN profil p on message.idProfil = p.idProfil LEFT JOIN theme ON message.idTheme = theme.idTheme WHERE message.idMessage = ".$idMessage." AND idMessage_1 IS NULL ORDER BY idMessage DESC;");

        if ($result->num_rows == 0) {
            ?>

            <div class="alert alert-warning"><strong>Oops... </strong> Aucun post sur ce thème...
            </div>

            <?php
        } else {

            while ($row = mysqli_fetch_array($result)) {
                $idTheme = $row["idTheme"];
                ?>

                <div class="post">
                    <div class="card mb-3">
                        <h3 class="card-header">Post de <a
                                    href="/social/profile.php?idProfil=<?php echo($row["idProfil"]); ?>"><?php echo($row["nomProfil"]); ?> </a> dans <?php echo($row["libelleTheme"]); ?>
                        </h3>

                        <div class="card-body">

                            <p class="card-text"><?php echo($row["contenuMessage"]); ?></p>

                            <small><span
                                        id="like-<?php echo($row["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($row["idMessage"])); ?></span>
                                j'aime
                            </small>
                        </div>

                        <div class="card-body">
                            <?php if (doILikeThis($row["idMessage"])) { ?>
                                <a class="btn btn-outline-success" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">Je n'aime plus</a>
                            <?php } else { ?>
                                <a class="btn btn-outline-primary" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">J'aime </a>
                            <?php } ?>


                        </div>

                        <div class="card-body">

                            <h4>Réponses du post</h4>
                            <div class="commentSection">
                                <h5>Votre commentaire</h5>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-10">

                                        <input id="text-reponse-<?php echo($row["idMessage"]); ?>" class="form-control comment-Text  "/>
                                    </div>
                                    <div class="col-md-2">
                                        <button onclick="sendComment(<?php echo($row["idMessage"]); ?>)" class="comment-Send btn btn-outline-primary">
                                            Envoyer
                                        </button>

                                    </div>
                                </div>
                            </div>
                            <br />

                            <?php

                            $reponses = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $row["idMessage"] . " ORDER BY idMessage DESC;");

                            if ($reponses->num_rows == 0) {

                                ?>

                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune réponse...
                                </div>

                                <?php

                            } else {

                                while ($rowRep = mysqli_fetch_array($reponses)) {
                                    ?>

                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1"><a
                                                        href="/social/profile.php?idProfil=<?php echo($rowRep["idProfil"]); ?>"><?php echo($rowRep["nomProfil"]); ?></a>
                                            </h5>
                                            <small class="text-muted"><?php echo(explode(" ", $rowRep["timestampMessage"])[0]); ?>
                                                à <?php echo(explode(" ", $rowRep["timestampMessage"])[1]); ?></small>
                                        </div>
                                        <p class="mb-1"><?php echo($rowRep["contenuMessage"]); ?></p>
                                        <small><span
                                                    id="like-<?php echo($rowRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRep["idMessage"])); ?></span>
                                            j'aime
                                        </small>
                                        <div class="card-body">
                                            <?php if (doILikeThis($rowRep["idMessage"])) { ?>
                                                <a class="btn btn-outline-success btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">Je n'aime
                                                    plus
                                                </a>

                                            <?php } else { ?>
                                                <a class="btn btn-outline-primary btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">J'aime
                                                </a>
                                            <?php } ?>

                                        </div>

                                        <div class="card-body">

                                            <h4>En réponse à ce commentaire</h4>
                                            <div class="commentSection">
                                                <h5>Votre commentaire</h5>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3 col-md-10">

                                                        <input id="text-reponse-<?php echo($rowRep["idMessage"]); ?>" class="form-control comment-Text"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button onclick="sendComment(<?php echo($rowRep["idMessage"]); ?>)" class="comment-Send btn btn-outline-primary">
                                                            Envoyer
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                            <br/>


                                            <?php

                                            $reponsesRep = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $rowRep["idMessage"] . " ORDER BY idMessage DESC;");

                                            if ($reponsesRep->num_rows == 0) {

                                                ?>

                                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune
                                                    réponse...
                                                </div>

                                                <?php

                                            } else {

                                                while ($rowRepRep = mysqli_fetch_array($reponsesRep)) {
                                                    ?>

                                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                                        <div class="d-flex w-100 justify-content-between">
                                                            <h5 class="mb-1"><a
                                                                        href="/social/profile.php?idProfil=<?php echo($rowRepRep["idProfil"]); ?>"><?php echo($rowRepRep["nomProfil"]); ?></a>
                                                            </h5>
                                                            <small class="text-muted"><?php echo(explode(" ", $rowRepRep["timestampMessage"])[0]); ?>
                                                                à <?php echo(explode(" ", $rowRepRep["timestampMessage"])[1]); ?></small>
                                                        </div>
                                                        <p class="mb-1"><?php echo($rowRepRep["contenuMessage"]); ?></p>
                                                        <small><span
                                                                    id="like-<?php echo($rowRepRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRepRep["idMessage"])); ?></span>
                                                            j'aime
                                                        </small>
                                                        <div class="card-body">
                                                            <?php if (doILikeThis($rowRepRep["idMessage"])) { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-success btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">Je
                                                                    n'aime plus</a>
                                                            <?php } else { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-primary btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">J'aime</a>
                                                            <?php } ?>

                                                        </div>


                                                    </div>

                                                <?php }
                                            } ?>


                                        </div>


                                    </div>

                                <?php }
                            } ?>


                        </div>
                        <div class="card-footer text-muted">
                            Posté le <?php echo(explode(" ", $row["timestampMessage"])[0]); ?>
                            à <?php echo(explode(" ", $row["timestampMessage"])[1]); ?>
                        </div>
                    </div>
                </div>

            <?php }
        } ?>


    </article>

</section>

<script>
    function likeMessage(idMessage) {
        $.post("/social/ajax/like.php",
            {
                idMessage: idMessage,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {

                    document.getElementById(idMessage).textContent = data["newText"];
                    document.getElementById("like-" + idMessage).textContent = data["nbLike"];

                }


            }
        );
    }

    function sendComment(idMessage) {
        $.post("/social/ajax/comment.php",
            {
                idMessage: idMessage,
                idTheme: <?php echo($idTheme); ?>,
                contenu: document.getElementById("text-reponse-"+idMessage).value,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {
                    if(data["refresh"] === true){
                        location.reload();
                    }
                }
                alert(data["alert"]);


            }
        );
    }


</script>

<?php require_once(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>

