<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:56
 */
require_once(__DIR__ . "/../php/database/connect.php");
require_once(__DIR__ . "/../php/functions/themes.php");
require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");

redirectIfnotLoggedIn();

@session_start();

if (!isset($_GET["idProfil"]) || empty($_GET["idProfil"])) {
    $idProfil = addslashes(htmlspecialchars($_SESSION["idProfil"]));
} else {
    $idProfil = addslashes(htmlspecialchars($_GET["idProfil"]));
}

$details = getProfileDetails($idProfil);


?>

<html>
<head>
    <title>Interraction avec les thèmes JUIN 2019</title>
    <?php require_once(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require_once(__DIR__ . "/../inc/nav.php"); ?>


<?php


if (!isset($_GET["triPrincipal"]) || empty($_GET["triPrincipal"])) {
    $triPri = "non";
    $triPriOK = false;
} else {
    $triPri = addslashes(htmlspecialchars($_GET["triPrincipal"]));
    $triPriOK = true;
}


if (!isset($_GET["triSecondaire"]) || empty($_GET["triSecondaire"])) {
    $triSec = "non";
    $triSecOK = false;
} else {
    $triSec = addslashes(htmlspecialchars($_GET["triSecondaire"]));
    $triSecOK = true;
}


if (!isset($_GET["croissPrincipal"]) || empty($_GET["croissPrincipal"])) {
    $croissPri = "ASC";
} else {
    if ($triPriOK) {
        if (!in_array($_GET["croissPrincipal"], array("ASC", "DESC"))) {
            $croissPri = "ASC";
        } else {

            $croissPri = addslashes(htmlspecialchars($_GET["croissPrincipal"]));
        }
    }
}


if (!isset($_GET["croissSecondaire"]) || empty($_GET["croissSecondaire"])) {
    $croissSec = "ASC";
} else {

    if ($triSecOK) {
        if (!in_array($_GET["croissSecondaire"], array("ASC", "DESC"))) {
            $croissSec = "ASC";
        } else {

            $croissSec = addslashes(htmlspecialchars($_GET["croissSecondaire"]));
        }
    }
}


if (!$triPriOK && !$triSecOK) {
    $orderBy = "ORDER BY cumul DESC, libelleTheme ASC";
} else {

    if ($triPriOK) {

        if ($triPri != "non") {

            if ($triPri == "theme") {
                $orderBy = "ORDER BY libelleTheme ";
            } elseif ($triPri == "date") {
                $orderBy = "ORDER BY dateLast ";
            } elseif ($triPri == "points") {
                $orderBy = "ORDER BY cumul ";
            } else {
                $orderBy = "";
            }

            $orderBy .= $croissPri;

        } else {
            $orderBy = "";
        }


        if ($triSecOK) {


            if ($triSec != "non") {

                if ($triPri != "non") {

                    if ($triSec == "theme") {
                        $orderBy .= ", libelleTheme ";
                    } elseif ($triSec == "date") {
                        $orderBy .= ", dateLast ";
                    } elseif ($triSec == "points") {
                        $orderBy .= ", cumul ";
                    } else {
                        $orderBy .= "";
                    }


                    $orderBy .= $croissSec;

                }
            } else {

                $orderBy .= "";
            }
        }
    }
}

$sql = "SELECT logo, libelleTheme,
MAX(timestampPartie) as dateLast, SUM(points) as cumul FROM profil
    INNER JOIN participer p on profil.idProfil = p.idProfil
    INNER JOIN partie p2 on p.idPartie = p2.idPartie
    INNER JOIN integrer i on p2.idPartie = i.idPartie
    INNER JOIN question q on i.idQuestion = q.idQuestion
    INNER JOIN repondre r on p2.idPartie = r.idPartie
        AND profil.idProfil = r.idProfil
        AND q.idQuestion = r.idQuestion
    INNER JOIN theme t on q.idTheme = t.idTheme
WHERE profil.idProfil = " . $idProfil . "
GROUP BY logo, libelleTheme
" . $orderBy . ";";



$result = mysqli_query($bdd, $sql);


?>


<section>
    <header>
        <h2>Liste des profils</h2>
    </header>
    <article>

        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
            <button type="button" class="btn btn-info">Liste des profils consultable</button>
            <div class="btn-group" role="group">
                <button id="btnGroupDrop3" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop3">

                    <?php

                    $listUser = mysqli_query($bdd, "SELECT * FROM profil JOIN usersession u on profil.idProfil = u.idProfil WHERE profilPrive = 0 OR profilPrive IS NULL");
                    while($row = mysqli_fetch_array($listUser)) {

                        ?>

                        <a class="dropdown-item"
                           href="/social/examen.php?idProfil=<?php echo($row["idProfil"]); ?>"><?php echo(getProfileDetails($row["idProfil"])["details"]["username"]); ?></a>

                        <?php

                    }

                    ?>
                </div>
            </div>
        </div>


    </article>
</section>

<?php if ($details["success"] == true) { ?>

    <?php if ($details["details"]["private"]) { ?>


        <section>
            <header>
                <div class="alert alert-warning">
                    Le profil avec `id` (<?php echo($idProfil); ?>) est privé.
                </div>
            </header>
        </section>


    <?php } else { ?>


        <section>
            <header>
                <h2>
                    <?php echo($details["details"]["username"]); ?> à interragi avec ces thèmes.
                </h2>
            </header>
            <article>

                <form>


                    <input name="idProfil" type="text" hidden value="<?php echo($idProfil); ?>">

                    <div class="form-group">
                        <label for="triPrincipal">Trier par...</label>
                        <select class="custom-select" id="triPrincipal" name="triPrincipal">
                            <option value="non">Pas de tri</option>
                            <option value="theme">Theme</option>
                            <option value="date">Date</option>
                            <option value="points">Points</option>
                        </select>
                    </div>

                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="croissPrincipal" id="option1" value="ASC"> Croissant
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="croissPrincipal" id="option2" value="DESC"> Décroissant
                        </label>
                    </div>

                    <br/>
                    <br/>


                    <div class="form-group">
                        <label for="triSecondaire">Mais aussi par...</label>
                        <select class="custom-select" id="triSecondaire" name="triSecondaire">
                            <option value="non">Pas de second tri</option>
                            <option value="theme">Theme</option>
                            <option value="date">Date</option>
                            <option value="points">Points</option>
                        </select>
                    </div>

                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="croissSecondaire" id="option3" value="DESC"> Croissant
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="croissSecondaire" id="option4" value="ASC"> Décroissant
                        </label>
                    </div>


                    <br/>
                    <br/>

                    <button type="submit" class="btn btn-primary">Trier !</button>


                </form>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Logo</th>
                        <th scope="col">Libellé du thème</th>
                        <th scope="col">Date dernière partie</th>
                        <th scope="col">Points cumulés</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php while ($row = mysqli_fetch_array($result)) { ?>


                        <tr>
                            <th><img src="<?php echo($row["logo"]); ?>"/></th>
                            <td><?php echo($row["libelleTheme"]); ?></td>
                            <td><?php echo($row["dateLast"]); ?></td>
                            <td><?php echo($row["cumul"]); ?></td>
                        </tr>


                    <?php } ?>

                    </tbody>
                </table>

            </article>
        </section>


    <?php } ?>

<?php } else { ?>


    <section>
        <header>
            <div class="alert alert-primary">
                Le profil avec `id` (<?php echo($idProfil); ?>) n'a pas été trouvé.
            </div>
        </header>
    </section>


<?php } ?>


<?php require_once(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>