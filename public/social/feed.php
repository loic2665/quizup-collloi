<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:56
 */
require_once(__DIR__ . "/../php/database/connect.php");
require_once(__DIR__ . "/../php/functions/themes.php");
require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");

redirectIfnotLoggedIn();




?>

<html>
<head>
    <title>Accueil</title>
    <?php require_once(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require_once(__DIR__ . "/../inc/nav.php"); ?>


<section>

    <header>
        <h2>Conaissez-vous...</h2>
    </header>

    <article>

        <div class="container">
            <div class="row">

                <?php

                $relatedUser = proposeRelatedUser($_SESSION["idProfil"]);
                foreach ($relatedUser as $user){

                    ?>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo($user["username"]); ?></h4>
                                <div>
                                    <img src="<?php echo($user["pic"]); ?>" alt="<?php echo($user["username"]); ?>" class="profilePic relatedUser">
                                </div>
                                <p>
                                    <br />
                                    <a href="/social/profile.php?idProfil=<?php echo($user["id"]); ?>" class="btn btn-info">Voir le profil</a>
                                </p>
                            </div>
                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>
    </article>

</section>



<section>
    <header>
        <h2>Fil d'actualité global  - <a href="./examen.php">Examen</a> </h2>
    </header>

    <article>
        <div class="post">
            <div class="commentSection">
                <h5>Votre post</h5>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-10">

                        <select class="form-control" id="idThemeSelected">
                            <option value="0" selected>Choisissez une option</option>
                            <?php

                            $result = mysqli_query($bdd, "SELECT * FROM suivre s LEFT JOIN theme t ON s.idTheme = t.idTheme WHERE s.idProfil = ".$_SESSION["idProfil"].";");

                            while($row = mysqli_fetch_array($result)){ ?>
                                <option value="<?php echo($row["idTheme"]); ?>"><?php echo($row["libelleTheme"]); ?></option>
                            <?php }

                            ?>
                        </select>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-10">

                        <input id="text-newpost" class="form-control comment-Text"/>
                    </div>
                    <div class="col-md-2">
                        <button onclick="sendComment(-1)" class="comment-Send btn btn-outline-primary">
                            Envoyer
                        </button>

                    </div>
                </div>
            </div>
        </div>

        <br />

        <?php


        $result = mysqli_query($bdd, "SELECT * FROM suivre WHERE idProfil = ".$_SESSION["idProfil"].";");
        $parent = "(";

        while($row = mysqli_fetch_array($result)){
            $parent .= $row["idTheme"].",";
        }
        $parent .= ")";

        $parent = str_replace(",)", ")", $parent);


        $result = mysqli_query($bdd, "SELECT * FROM message LEFT JOIN profil p on message.idProfil = p.idProfil LEFT JOIN theme ON message.idTheme = theme.idTheme WHERE message.idTheme IN ".$parent." AND idMessage_1 IS NULL ORDER BY idMessage DESC;");

        if (!$result) {
            ?>

            <div class="alert alert-warning"><strong>Oops... </strong> Aucun post sur ce thème...
            </div>

            <?php
        } else {

            while ($row = mysqli_fetch_array($result)) {
                ?>

                <div class="post">
                    <div class="card mb-3">
                        <h3 class="card-header">Post de <?php echo($row["nomProfil"]); ?> dans <?php echo($row["libelleTheme"]); ?> - <a href="./post.php?idPost=<?php echo($row["idMessage"]); ?>">Voir le post seul</a>
                        </h3>

                        <div class="card-body">

                            <p class="card-text"><?php echo($row["contenuMessage"]); ?></p>

                            <small><span
                                        id="like-<?php echo($row["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($row["idMessage"])); ?></span>
                                j'aime
                            </small>
                        </div>

                        <div class="card-body">
                            <?php if (doILikeThis($row["idMessage"])) { ?>
                                <a class="btn btn-outline-success" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">Je n'aime plus</a>
                            <?php } else { ?>
                                <a class="btn btn-outline-primary" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">J'aime </a>
                            <?php } ?>


                        </div>

                        <div class="card-body">

                            <h4>Réponses du post</h4>


                            <?php

                            $reponses = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $row["idMessage"] . " ORDER BY idMessage DESC;");

                            if ($reponses->num_rows == 0) {

                                ?>

                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune réponse...
                                </div>

                                <?php

                            } else {

                                while ($rowRep = mysqli_fetch_array($reponses)) {
                                    ?>

                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1"><?php echo($rowRep["nomProfil"]); ?>
                                            </h5>
                                            <small class="text-muted"><?php echo(explode(" ", $rowRep["timestampMessage"])[0]); ?>
                                                à <?php echo(explode(" ", $rowRep["timestampMessage"])[1]); ?></small>
                                        </div>
                                        <p class="mb-1"><?php echo($rowRep["contenuMessage"]); ?></p>
                                        <small><span
                                                    id="like-<?php echo($rowRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRep["idMessage"])); ?></span>
                                            j'aime
                                        </small>
                                        <div class="card-body">
                                            <?php if (doILikeThis($rowRep["idMessage"])) { ?>
                                                <a class="btn btn-outline-success btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">Je n'aime
                                                    plus
                                                </a>

                                            <?php } else { ?>
                                                <a class="btn btn-outline-primary btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">J'aime
                                                </a>
                                            <?php } ?>

                                        </div>

                                        <div class="card-body">

                                            <h4>En réponse à ce commentaire</h4>



                                            <?php

                                            $reponsesRep = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $rowRep["idMessage"] . " ORDER BY idMessage DESC;");

                                            if ($reponsesRep->num_rows == 0) {

                                                ?>

                                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune
                                                    réponse...
                                                </div>

                                                <?php

                                            } else {

                                                while ($rowRepRep = mysqli_fetch_array($reponsesRep)) {
                                                    ?>

                                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                                        <div class="d-flex w-100 justify-content-between">
                                                            <h5 class="mb-1"><?php echo($rowRepRep["nomProfil"]); ?>
                                                            </h5>
                                                            <small class="text-muted"><?php echo(explode(" ", $rowRepRep["timestampMessage"])[0]); ?>
                                                                à <?php echo(explode(" ", $rowRepRep["timestampMessage"])[1]); ?></small>
                                                        </div>
                                                        <p class="mb-1"><?php echo($rowRepRep["contenuMessage"]); ?></p>
                                                        <small><span
                                                                    id="like-<?php echo($rowRepRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRepRep["idMessage"])); ?></span>
                                                            j'aime
                                                        </small>
                                                        <div class="card-body">
                                                            <?php if (doILikeThis($rowRepRep["idMessage"])) { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-success btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">Je
                                                                    n'aime plus</a>
                                                            <?php } else { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-primary btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">J'aime</a>
                                                            <?php } ?>

                                                        </div>


                                                    </div>

                                                <?php }
                                            } ?>


                                        </div>


                                    </div>

                                <?php }
                            } ?>


                        </div>
                        <div class="card-footer text-muted">
                            Posté le <?php echo(explode(" ", $row["timestampMessage"])[0]); ?>
                            à <?php echo(explode(" ", $row["timestampMessage"])[1]); ?>
                        </div>
                    </div>
                </div>

            <?php }
        } ?>


    </article>

</section>

<script>


    var likeMsg = new Audio('/assets/like.ogg');
    var dislikeMsg = new Audio('/assets/dislike.ogg');

    function likeMessage(idMessage) {
        $.post("/social/ajax/like.php",
            {
                idMessage: idMessage,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {

                    document.getElementById(idMessage).textContent = data["newText"];
                    document.getElementById("like-" + idMessage).textContent = data["nbLike"];

                    if(data["newText"] === "Je n'aime plus"){

                        likeMsg.play();
                    }else{

                        dislikeMsg.play();
                    }

                }


            }
        );
    }

    function sendComment(idMessage) {
        $.post("/social/ajax/comment.php",
            {
                idMessage: idMessage,
                idTheme: $('#idThemeSelected').val(),
                contenu: document.getElementById("text-newpost").value,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {
                    if(data["refresh"] === true){
                        location.reload();
                    }
                }
                alert(data["alert"]);


            }
        );
    }


</script>

<?php require_once(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>

